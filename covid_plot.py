#!/usr/bin/env python3

import csv
import numpy as np
import numba as nb
import pandas as pd
import matplotlib.pyplot as plt


def forward_fill_zeros(arr):
    """
    Forward fills 1D numpy arrays
    :param arr:
    :return:
    """
    idx = np.where(arr != 0, np.arange(arr.shape[0]), 0)
    np.maximum.accumulate(idx, axis=None, out=idx)
    out = arr[idx]
    return out.astype(int)


if __name__ == '__main__':
    print("Ingesting data from time_series_covid19_confirmed_US.csv")
    contra_costa = {}
    with open("COVID-19/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        contra_costa = [x for x in reader if "contra costa" in str(x["Admin2"]).lower() and "california" in str(x["Province_State"]).lower()]

    # Convert dict indices after 'Combined_Key' into a list of confirmed case counts
    date_start_index = list(contra_costa[0].keys()).index("Combined_Key")

    # This may be O(n^2) but the list is small
    # TODO: Optimise this
    total_confirmed_cases = np.asarray([int(contra_costa[0][k]) for k in contra_costa[0] if list(contra_costa[0].keys()).index(k) > date_start_index])

    first_occurrence_index = (total_confirmed_cases != 0).argmax(axis=0)

    # Get delta
    new_confirmed_cases = np.diff(total_confirmed_cases)

    # forward fill 0 values of Y after first occurrence to remove noisy data
    new_confirmed_cases_ff = forward_fill_zeros(new_confirmed_cases)

    # new_confirmed_cases = [np.nan if x==0 else x for x in new_confirmed_cases]

    # Plot new_confirmed_cases vs total_confirmed_cases on log scale.
    # Remove first of total_confirmed_cases to match new

    plt.semilogx(total_confirmed_cases[1:], new_confirmed_cases_ff, nonposx='clip')
    # plt.xscale("log")
    plt.grid(True)
    plt.xlabel("Total Confirmed Cases")
    plt.ylabel("New Confirmed Cases")
    plt.title('Log Graph of Total Confirmed Cases vs. New Confirmed Cases')
    plt.show()

    # For debug
    print("total_confirmed_cases:\n", total_confirmed_cases)
    print("new_confirmed_cases:\n", new_confirmed_cases)
    print("new_confirmed_cases_ff:\n", new_confirmed_cases_ff)
    print("Done")
