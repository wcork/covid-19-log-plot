#!/usr/bin/env bash

# If we set it as a submodule
# git submodule update --remote --merge

REPO_URL=https://github.com/CSSEGISandData/COVID-19

if [ -d "COVID-19" ]
then
  echo "Repo exists. Updating."
  cd COVID-19 && git pull
else
  echo "Repo does not exist. Cloning repo."
  git clone $REPO_URL
fi
